// ----------------
// RunAllocator.cpp
// ----------------

// --------
// includes
// --------


#include <iostream> // cin, cout

#include "Allocator.hpp"
#include <string>   // string
#include <vector>
using namespace std;

// ----
// main
// ----


string getOutput() { // get string output for allocation and deallocation of a block
    string s;
    My_Allocator<double, 1000> temp;
    vector<double*> storage(1000, nullptr);
    while (true) {
        getline(cin, s);
        // cout << s << endl;
        if (s.empty() || !cin) {
            // cout << "broke loop" << endl;
            break;
        }
        int num = stoi(s);
        // cout << num << endl;
        if (num > 0) { // allocate
            temp.allocate(num);
            // cout << "allocated" << endl;
        } else { // deallocate

            int blk_count = 0;
            int de_block = abs(num);

            My_Allocator<double, 1000>::iterator curr = temp.begin();
            My_Allocator<double, 1000>::iterator e = temp.end();
            while (curr != e && blk_count != de_block) {
                if (*curr < 0) {
                    ++blk_count;
                }
                ++curr;
            }
            if (blk_count < de_block) {
                continue;
            }
            --curr;
            // we are at the front sentinal
            int& block_fsent = *curr; // block size of block we are planning on deallocating
            temp.deallocate(reinterpret_cast<double*>((reinterpret_cast<char*>(&block_fsent) + sizeof(int))), 0);
        }
    }

    string out = "";
    // MyAllocator<int, 1000>::iterator it = temp.begin();
    for (My_Allocator<double, 1000>::iterator temp_it = temp.begin(); temp_it != temp.end(); ++temp_it) { // access block sentinals and convert to string output
        out += to_string(*temp_it) + " ";
    }
    // cout << "returning string" << endl;
    return out.substr(0, out.length() - 1);
}


int main () {

    // the acceptance tests are hardwired to use My_Allocator<double, 1000>
    // read newline
    string s;
    getline(cin, s);
    int numCases = stoi(s);

    getline(cin, s);

    // read, eval, print (REPL)
    for (int i = 0; i < numCases; ++i) {
        string out = getOutput();
        cout << out << endl;
    }
    return 0;
}

