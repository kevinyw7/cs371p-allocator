# CS371p: Object-Oriented Programming Allocator Repo

* Name: Kevin Wang

* EID: kyw245

* GitLab ID: kevinyw7

* HackerRank ID: kevinyw7

* Git SHA: ed20a97e1396a32793901504e4fca06b8d002920

* GitLab Pipelines: https://gitlab.com/kevinyw7/cs371p-allocator/-/jobs/6494921090

* Estimated completion time: 15 hours

* Actual completion time: 20 hours

* Comments: None
