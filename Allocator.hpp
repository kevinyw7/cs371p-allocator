// -------------
// Allocator.hpp
// -------------

#ifndef Allocator_hpp
#define Allocator_hpp

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream>
#include <string>
#include <cmath> // mainly for absolute value
using namespace std; // standard library

// ------------
// My_Allocator
// ------------

template <typename T, std::size_t N>
class My_Allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const My_Allocator&, const My_Allocator&) { // this is correct
        return false;
    }

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const My_Allocator& lhs, const My_Allocator& rhs) { // this is correct
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using value_type      = T;

    using size_type       = std::size_t;
    using difference_type = std::ptrdiff_t;

    using pointer         =       value_type*;
    using const_pointer   = const value_type*;

    using reference       =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) { // fix!
            // standard equals?
            // <your code>

            // return equal(lhs._r.begin(), lhs._r.end(), rhs._r.begin()) && lhs._i == rhs._i;
            return lhs._i == rhs._i;

            // return ((lhs._r == rhs._r) && (lhs._i == rhs._i));
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) { // this is correct
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        My_Allocator& _r;
        std::size_t   _i;

    public:
        // -----------
        // constructor
        // -----------

        iterator (My_Allocator& r, size_type i) :
            _r (r),
            _i (i)
        {}

        // ----------
        // operator *
        // ----------

        /**
         * beginning sentinel of the block
         */
        int& operator * () const { // fix!
            // <your code>
            // int* val = (int*)_r.a[_i];
            // return *val;

            // int tmp = (int)_r.a[_i];
            // return tmp;
            return _r[_i];

        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () { // fix!
            // <your code>

            if (_i == N) { // edge case: already at end?
                return *this;
            }
            // iterator tmp = *this;
            // _i = _i + abs(*tmp) + 8; // size given by sentinal + 4 (size of ending sentinal)
            _i += abs(_r[_i]) + 8;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) { // this is correct
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () { // fix!
            // <your code>

            if (_i == 0) { // edge case: at beginning?
                return *this;
            }
            // iterator tmp = *this;
            // _i = _i - abs(*tmp) - 8; // size given by sentinal + 4 (size of ending sentinal)
            _i = _i - abs(_r[_i - 4]) - 8;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) { // this is correct
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) { // fix!
            // <your code>
            // return equal(lhs.begin(), lhs.end(), rhs.begin());

            // return equal(lhs._r.begin(), lhs._r.end(), rhs._r.begin()) && lhs._i == rhs._i;
            return lhs._i == rhs._i;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) { // this is correct
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const My_Allocator& _r;
        std::size_t         _i;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const My_Allocator& r, size_type i) :
            _r (r),
            _i (i)
        {}

        // ----------
        // operator *
        // ----------

        // beginning sentinel of the block
        const int& operator * () const { // fix!
            // <your code>

            // return (int)_r.a[_i];

            return _r[_i];
            // static int tmp = 0;
            // return tmp;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () { // fix!
            // <your code>

            if (_i == N) { // edge case: already at end?
                return *this;
            }
            // const_iterator tmp = *this;
            // _i = _i + abs(*tmp) + 8; // size given by sentinal + 4 (size of ending sentinal)
            _i += abs(_r[_i]) + 8;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) { // this is correct
            const_iterator tmp = *this;
            ++*this;
            return tmp;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () { // fix!
            // <your code>

            if (_i == 0) { // edge case: at beginning?
                return *this;
            }
            // _i = _i - abs(*this) - 8; // size given by sentinal + 4 (size of ending sentinal)
            // return *this;}
            _i = _i - abs(_r[_i - 4]) - 8;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) { // this is correct
            const_iterator tmp = *this;
            --*this;
            return tmp;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N]; // array of bytes

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     * use allocator's iterator to check if the sentinal displays correct sizes in each block
     */

    bool valid () const {
        // <your code>
        // <you must use allocator's iterators>

        const_iterator curr = begin();
        const_iterator e = end();
        for (; curr != e; ++curr) {
            const int& curr_sent = *curr;
            if(curr_sent != *reinterpret_cast<const int*>((reinterpret_cast<const char*>(&curr_sent) + abs(*curr) + sizeof(int)))) { // check sentinals with size
                return false;
            }
        }
        return true;
    }


public:

    string printa() { // print method for debugging, remove later

        string out = "";
        iterator temp = begin();
        for (; temp != end(); ++temp) {
            int val = *temp;
            out += to_string(val) + " ";
        }
        return out;
    }

    // -----------
    // constructor
    // -----------


    /**
     * O(1) in space
     * O(1) in time
     * throw a std::bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    My_Allocator () {
        if (N < (sizeof(T) + (2 * sizeof(int))))
            throw std::bad_alloc();
        (*this)[0]   = N-8;
        (*this)[N-4] = N-8;
        assert(valid());
    }

    My_Allocator             (const My_Allocator&) = default;
    ~My_Allocator            ()                    = default;
    My_Allocator& operator = (const My_Allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a std::bad_alloc exception, if there isn't an acceptable free block
     */



    pointer allocate (size_type s) {
        // <you must use allocator's iterators>
        assert(valid());
        // min size for one block allocation (no split)
        int size_for_one = s * sizeof(T);
        // min size where allocating will split a free block into two blocks
        int size_for_two = size_for_one + sizeof(T) + (2 * sizeof(int));

        iterator curr = begin();
        iterator e = end();
        for(; curr != e; ++curr) { // iterate through blocks
            if (*curr >= size_for_one) { // found a valid free block, break
                break;
            }
        }

        int& block_fsent = *curr; // block size of block we are planning on allocating
        int spaceLeft = block_fsent - size_for_one;
        if (block_fsent >= size_for_two) { // split a free block into two blocks
            block_fsent = -1 * size_for_one; // block one new beginning sentinal
            *reinterpret_cast<int*>((reinterpret_cast<char*>(&block_fsent) + size_for_one + sizeof(int))) = -1 * size_for_one; // block 1 new end sentinel

            pointer user_pointer = reinterpret_cast<value_type*>((reinterpret_cast<char*>(&block_fsent) + sizeof(int)));
            ++curr; // move to location of block 2 (free block)

            int& block2_fsent = *curr;
            int block2_space = spaceLeft - (2 * sizeof(int));
            block2_fsent = block2_space; // free block front sentinal
            *reinterpret_cast<int*>((reinterpret_cast<char*>(&block2_fsent) + block2_fsent + sizeof(int))) = block2_space; // free block end sentinal
            assert(valid());
            return user_pointer;

        } else { // allocate the entire block
            block_fsent *= -1; // negate front sentinal
            *reinterpret_cast<int*>((reinterpret_cast<char*>(&block_fsent) - block_fsent + sizeof(int))) *= -1; // negate ending sentinel
            pointer user_pointer = reinterpret_cast<value_type*>(reinterpret_cast<char*>(&block_fsent) + sizeof(int));
            assert(valid());
            return user_pointer;
        }
        // no acceptable free block found
        throw std::bad_alloc();
    }



    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) { // this is correct and exempt
        new (p) T(v);                               // from the prohibition of new
        assert(valid());
    }

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */

    void deallocate (pointer p, size_type) {
        if (p == NULL) throw invalid_argument("null pointer");
        if (p < reinterpret_cast<value_type*>(&(*this)[0]) || p >= reinterpret_cast<value_type*>(&(*this)[N])) throw std::invalid_argument("pointer out of bounds");

        int& curr_fsent = *(reinterpret_cast<int*>(p) - 1); // point to the block's beginning sentinel
        int& curr_esent = *reinterpret_cast<int*>(reinterpret_cast<char*>(&curr_fsent) + abs(curr_fsent) + sizeof(int)); // block's ending sentinel
        if (curr_fsent >= 0 || curr_esent >= 0 || curr_fsent != curr_esent) { // ensure that the block is busy (negative sentinels) and valid (sentinels match in value)
            throw invalid_argument("invalid pointer");
        }

        int& prev_esent = *(&curr_fsent - 1); // previous adjacent block's ending sentinel
        int& next_fsent = *(&curr_esent + 1); // next adjacent block's beginning sentinel
        bool hasPrev = &prev_esent > reinterpret_cast<int*>(&(*this)[0]) && prev_esent > 0; // check if previous adjacent block is present and is free
        bool hasNext = &next_fsent < reinterpret_cast<int*>(&(*this)[N]) && next_fsent > 0; // check if next adjacent block is present and is free

        if(hasPrev && hasNext) {
            // both adjacent blocks are free
            int& prev_bsent = *reinterpret_cast<int*>(reinterpret_cast<char*>(&prev_esent) - prev_esent - sizeof(int));
            int& next_esent = *reinterpret_cast<int*>(reinterpret_cast<char*>(&next_fsent) + next_fsent + sizeof(int));
            int totalSize = prev_bsent - curr_fsent + next_esent + 4 * sizeof(int);
            prev_bsent = totalSize;
            next_esent = totalSize;
        }
        else if(hasPrev) {
            // only the previous adjacent block is free
            int totalSize = -curr_fsent + prev_esent + 2 * sizeof(int);
            int& prev_bsent = *reinterpret_cast<int*>(reinterpret_cast<char*>(&prev_esent) - prev_esent - sizeof(int));
            prev_bsent = totalSize;
            curr_esent = totalSize;
        }
        else if(hasNext) {
            // only the next adjacent block is free
            int totalSize = -curr_fsent + next_fsent + 2 * sizeof(int);
            int& next_esent = *reinterpret_cast<int*>(reinterpret_cast<char*>(&next_fsent) + next_fsent + sizeof(int));
            curr_fsent = totalSize;
            next_esent = totalSize;
        }
        else {
            // just make sentinals positive
            curr_fsent *= -1;
            curr_esent *= -1;
        }
        assert(valid());
    }




    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) { // this is correct
        p->~T();
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) { // this is correct
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const { // this is correct
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () { // this is correct
        return iterator(*this, 0);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const { // this is correct
        return const_iterator(*this, 0);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () { // this is correct
        return iterator(*this, N);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const { // this is correct
        return const_iterator(*this, N);
    }
};



#endif // Allocator_hpp
