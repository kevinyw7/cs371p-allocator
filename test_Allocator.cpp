// -----------------
// TestAllocator.cpp
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <string>    // string
#include <iostream>
#include <stdio.h>

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace std;

struct A {
    friend bool operator == (const A&, const A&) {
        A::log += "==(A, A) ";
        return true;
    }

    static string log;

    A  ()         = default;
    A  (int)      {
        log += "A(int) ";
    }
    A  (const A&) {
        log += "A(A) ";
    }
    ~A ()         {
        log += "~A() ";
    }
};

string A::log;


// allocate big block, allocated all, passed
TEST (myTest, test0) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);
    const pointer   e = b + s;
    const value_type v = 0;
    pointer p = b;
    while (p != e) {
        x.construct(p, v);
        ++p;
    }

    // string result = x.printa();
    // cout << result << endl;

    ASSERT_EQ(x[  0], -8);
    ASSERT_EQ(x[  12], -8);
    ASSERT_EQ(x[  16], 976);
    ASSERT_EQ(x[  996], 976);

    p = e;
    while (b != p) {
        --p;
        x.destroy(p);
    }
}

// allocate, split to two blocks, passed
TEST (myTest, test1) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 124;
    const pointer   b = x.allocate(s);
    const pointer   e = b + s;
    const value_type v = 0;
    pointer p = b;

    // string result = x.printa();
    // cout << result << endl;

    ASSERT_EQ(x[  0], -992);
    ASSERT_EQ(x[  996], -992);
}

// allocate, then deallocate and coalesce, passed
TEST (myTest, test2) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);
    const pointer   e = b + s;
    const value_type v = 0;

    // string result = x.printa();
    // cout << result << endl;

    x.deallocate(b, s);

    ASSERT_EQ(x[  0], 992);
    ASSERT_EQ(x[  996], 992);
}

// allocate twice
TEST (myTest, test3) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);
    const pointer   c = x.allocate(s);

    ASSERT_EQ(x[  0], -8);
    ASSERT_EQ(x[  12], -8);
    ASSERT_EQ(x[  16], -8);
    ASSERT_EQ(x[  28], -8);
    ASSERT_EQ(x[  32], 960);
    ASSERT_EQ(x[  996], 960);
}

// allocate twice, then deallocate first
TEST (myTest, test4) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);
    const pointer   c = x.allocate(s);
    x.deallocate(b, s);

    ASSERT_EQ(x[  0], 8);
    ASSERT_EQ(x[  12], 8);
    ASSERT_EQ(x[  16], -8);
    ASSERT_EQ(x[  28], -8);
    ASSERT_EQ(x[  32], 960);
    ASSERT_EQ(x[  996], 960);
}

// allocate twice, then deallocate second (two blocks should coalesce)
TEST (myTest, test5) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);
    const pointer   c = x.allocate(s);
    x.deallocate(c, s);

    ASSERT_EQ(x[  0], -8);
    ASSERT_EQ(x[  12], -8);
    ASSERT_EQ(x[  16], 976);
    ASSERT_EQ(x[  996], 976);
}

// allocate twice, then deallocate twice (three blocks should coalesce)
TEST (myTest, test6) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);
    const pointer   c = x.allocate(s);
    x.deallocate(b, s);
    x.deallocate(c, s);

    ASSERT_EQ(x[  0], 992);
    ASSERT_EQ(x[996], 992);
}

// allocate twice, then deallocate twice (deallocation order swapped from test 6) (three blocks should coalesce)
TEST (myTest, test7) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);
    const pointer   c = x.allocate(s);
    x.deallocate(c, s);
    x.deallocate(b, s);

    ASSERT_EQ(x[  0], 992);
    ASSERT_EQ(x[996], 992);
}

// basic iterator test
TEST (myTest, test8) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    allocator_type::iterator x_it= x.begin();
    ASSERT_EQ(*x_it, 992);

}

// iterator test, preincrement: allocate big block, allocated all, passed
TEST (myTest, test9) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);

    allocator_type::iterator x_it= x.begin();
    ASSERT_EQ(*x_it, -8);
    ASSERT_EQ(*++x_it, 976);
}

// iterator test, postincrement: allocate big block, allocated all, passed
TEST (myTest, test10) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);

    allocator_type::iterator x_it= x.begin();
    ASSERT_EQ(*x_it, -8);
    ASSERT_EQ(*x_it++, -8);
    ASSERT_EQ(*x_it, 976);
}

// iterator test, equality checks
TEST (myTest, test11) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    allocator_type::iterator x_it= x.begin();
    allocator_type::iterator y_it= x.begin();

    allocator_type::iterator x_it_end= x.end();

    ASSERT_EQ(x_it == y_it, true);
    ASSERT_EQ(x_it != y_it, false);

    ASSERT_EQ(x_it == x_it_end, false);
    ASSERT_EQ(x_it != x_it_end, true);
}

// iterator test, predecrement: allocate big block, allocated all, passed
TEST (myTest, test12) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);

    allocator_type::iterator x_it= x.end();

    ASSERT_EQ(*--x_it, 976);
    ASSERT_EQ(*--x_it, -8);
}

// iterator test, postdecrement: allocate big block, allocated all, passed
TEST (myTest, test13) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);

    allocator_type::iterator x_it= x.end();
    x_it--;
    ASSERT_EQ(*x_it--, 976);
    ASSERT_EQ(*x_it--, -8);
}

// allocate thrice
TEST (myTest, test14) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);
    const pointer   c = x.allocate(s);
    const pointer   d = x.allocate(s);

    ASSERT_EQ(x[  0], -8);
    ASSERT_EQ(x[  12], -8);
    ASSERT_EQ(x[  16], -8);
    ASSERT_EQ(x[  28], -8);
    ASSERT_EQ(x[  32], -8);
    ASSERT_EQ(x[  44], -8);

    ASSERT_EQ(x[  48], 944);
    ASSERT_EQ(x[  996], 944);
}

// allocate thrice, deallocate and coalesce first two blocks
TEST (myTest, test15) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);
    const pointer   c = x.allocate(s);
    const pointer   d = x.allocate(s);
    x.deallocate(b,s);
    x.deallocate(c,s);
    ASSERT_EQ(x[  0], 24);
    ASSERT_EQ(x[  28], 24);
    // ASSERT_EQ(x[  16], -8);
    // ASSERT_EQ(x[  28], -8);
    ASSERT_EQ(x[  32], -8);
    ASSERT_EQ(x[  44], -8);

    ASSERT_EQ(x[  48], 944);
    ASSERT_EQ(x[  996], 944);
}

// allocate thrice, deallocate and coalesce middle two blocks (three blocks will actually coalesce)
TEST (myTest, test16) {
    using allocator_type = My_Allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;

    const size_type s = 1;
    const pointer   b = x.allocate(s);
    const pointer   c = x.allocate(s);
    const pointer   d = x.allocate(s);
    x.deallocate(c,s);
    x.deallocate(d,s);

    ASSERT_EQ(x[  0], -8);
    ASSERT_EQ(x[  12], -8);
    ASSERT_EQ(x[  16], 976);
    ASSERT_EQ(x[  996], 976);
}






// passing
TEST(AllocatorFixture, test0) {
    using allocator_type = My_Allocator<A, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type  x;
    const size_type s = 2;
    const pointer   b = x.allocate(s);
    const pointer   e = b + s;

    const value_type v = 0;
    ASSERT_EQ(A::log, "A(int) ");

    pointer p = b;
    while (p != e) {
        x.construct(p, v);
        ++p;
    }
    ASSERT_EQ(A::log, "A(int) A(A) A(A) ");

    ASSERT_EQ(count(b, e, v), ptrdiff_t(s));
    ASSERT_EQ(A::log, "A(int) A(A) A(A) ==(A, A) ==(A, A) ");

    p = e;
    while (b != p) {
        --p;
        x.destroy(p);
    }
    ASSERT_EQ(A::log, "A(int) A(A) A(A) ==(A, A) ==(A, A) ~A() ~A() ");
    x.deallocate(b, s);
}

// passing
TEST(AllocatorFixture, test1) {
    using allocator_type = My_Allocator<A, 1000>;

    allocator_type x;        // read/write
    ASSERT_EQ(x[  0], 992);
    ASSERT_EQ(x[996], 992);
}

// passing
TEST(AllocatorFixture, test2) {
    using allocator_type = My_Allocator<A, 1000>;

    const allocator_type x;  // read-only
    ASSERT_EQ(x[  0], 992);
    ASSERT_EQ(x[996], 992);
}

